var _bookmarks = [];
var _parents = [];

var BmApp = angular.module('BmApp',[]);

BmApp.config(['$compileProvider',function( $compileProvider ){
        $compileProvider.imgSrcSanitizationWhitelist(/^\s*(https?|ftp|mailto|chrome|chrome-extension):/);
}]);

BmApp.controller('BookMarksCtrl', ['$scope', function($scope){

  $scope.bookmarks = [];
  $scope.selected = [];
  $scope.Bookmark = {id:0};
  $scope.SearchObject = {title: 'Snacks'}
  $scope._searchedBefore = false;
  $scope.SortBy = {attr: 'dateAdded',direction: 'dsc'}

  // Thanks to this answer http://stackoverflow.com/a/11582013/266636 for the pagination :)
  $scope.currentPage = 0;
  $scope.pageSize = 10;

  $scope.numberOfPages=function(){
      return Math.ceil($scope.bookmarks.length/$scope.pageSize);
  }

  $scope.getChromeBm = function(){
  // This is a resetter, so we always need to clean the cache variable
  _bookmarks = [];
  chrome.bookmarks.getTree(function(tree){
    flattenArray(tree);
    $scope.bookmarks = $scope.cachedBm();
    $scope.parents = _parents;
    $scope.$apply();
  })
  }

  $scope.cachedBm = function(){
    arr = _.sortBy(_bookmarks,$scope.SortBy.attr);
    if($scope.SortBy.direction == 'dsc'){
      arr = arr.reverse();
    };
    return arr;
  }

  $scope.sortCachedBm = function(){
    $scope.bookmarks = $scope.cachedBm();
  }

  $scope.removeBookmark = function(bookmark,e){
    isItOk = confirm("Are you sure? this can't be undone!");
    if(isItOk){
      chrome.bookmarks.remove(bookmark.id)
      $("#bookmark_" + bookmark.id).fadeOut();
      $scope.getChromeBm();
    }
  }

  $scope.getSelected = function(bookmark){
    return _.filter($scope.bookmarks,function(i){
      return i.Selected ? true : false;
    })
  }

  $scope.deleteSelected = function(){
    isItOk = confirm("Are you sure? this can't be undone!");
    if(!isItOk) return false;

    _.each($scope.getSelected(),function(bm){
      chrome.bookmarks.remove(bm.id)
      $("#bookmark_" + bm.id).fadeOut();
    });
    $scope.getChromeBm();
  }

  $scope.selectAll = function(){
    _.each($scope.bookmarks,function(bm){
      bm.Selected = true;
    });
  }

  $scope.deSelectAll = function(){
    _.each($scope.getSelected(),function(bm){
      bm.Selected = false;
    });
  }

  $scope.editBookmark = function(bookmark){
    // I want the instant edit effect.
    $scope.Bookmark = bookmark;
  }

  // This is an example of a very unoptimized function that might leak your memory till it bleeds to death ;)
  $scope.searchBookmarks = function(q){
    if(!q){
      if($scope._searchedBefore == true){
        $scope.bookmarks = $scope.cachedBm();
      }else{
        return false;
      }
    }else{
      $scope._searchedBefore = true;
      $scope.bookmarks = _.filter($scope.cachedBm(),function(d){
        var qPattern = new RegExp(q);
        if(qPattern.test(d.title)||qPattern.test(d.url)){
          return true;
        }
      }); //End of filter
    }
  }

  $scope.updateBookmark = function(bookmark,e){
    e.preventDefault();
    if(!$scope.editBookMarkForm.$valid)
      return false;

    if(bookmark.id == 0){
      // New Bookmark
      delete(bookmark.id);
      chrome.bookmarks.create(bookmark);
    }else{
      // Update bookmark
      chrome.bookmarks.update(bookmark.id,{title: bookmark.title, url: bookmark.url});
    }

    $scope.Bookmark = {id:0};
  }

  $scope.getChromeBm();
}]) // End of Controller

BmApp.filter('startFrom', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

BmApp.filter('searchFilter', function() {
    return function(input, start) {
        start = +start; //parse to int
        return input.slice(start);
    }
});

BmApp.filter('shorten', function() {
    return function(input,maxLen) {
        if(maxLen == undefined) maxLen = 50;
        if(input.length < maxLen) return input;
        return input.substr(0,maxLen) + ' ...';
    }
});

BmApp.filter('findParent', function() {
    return function(input) {
        parent = _.find(_parents,function(p){
          if(p.id == input) return true;
        });
        return parent.title;
    }
});


function flattenArray(arr){
  _.each(arr,function(parent){
    if(parent.children){
      _parents.push(parent);
      flattenArray(parent.children);
    }else{
      _bookmarks.push(parent);
    }
  });
}

